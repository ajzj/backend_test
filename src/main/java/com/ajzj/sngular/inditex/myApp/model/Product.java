package com.ajzj.sngular.inditex.myApp.model;

import lombok.Data;

@Data
public class Product {

    private String id;
    private String name;
    private String price;
    private Boolean availability;
}
