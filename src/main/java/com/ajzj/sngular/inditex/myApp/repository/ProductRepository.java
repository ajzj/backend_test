package com.ajzj.sngular.inditex.myApp.repository;

import com.ajzj.sngular.inditex.myApp.entity.ProductEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

/**
 * Products Repository
 * {@link ProductRepository }
 *
 * @author antonio
 */
@Slf4j
@Repository
public class ProductRepository {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${app.product-repository.detail}")
    private String urlDetail;

    @Value("${app.product-repository.similar}")
    private String urlSimilar;

    /**
     * Get Products Similar
     *
     * @param id
     * @return ProductEntity
     */
    public ProductEntity getProductDetail(Integer id) {

        ResponseEntity<ProductEntity> entity = null;
        try {
            entity = restTemplate.getForEntity(urlDetail+"product/"+id, ProductEntity.class);
            log.info("Entity --> {}", entity);
            return entity.getBody();
        } catch (Exception ex) {
            log.error("Product not found {}!!: {} ", id, ex.toString());
            return null;
        }
    }

    /**
     * Get List ids similar
     * @param id
     * @return List<Integer>
     */
    public Integer[] getProductsSimilar(Integer id){
        try {
            ResponseEntity<Integer[]> ids = restTemplate.getForEntity(urlSimilar+"product/"+id+"/similarids", Integer[].class);
            log.info("List Similar --> {}", ids.toString());
            return ids.getBody();
        } catch (Exception ex) {
            log.error("List error {}!!: {} ", id, ex.toString());
            return new Integer[]{};
        }

    }

}
