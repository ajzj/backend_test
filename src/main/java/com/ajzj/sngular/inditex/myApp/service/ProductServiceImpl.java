package com.ajzj.sngular.inditex.myApp.service;

import com.ajzj.sngular.inditex.myApp.entity.ProductEntity;
import com.ajzj.sngular.inditex.myApp.model.Product;
import com.ajzj.sngular.inditex.myApp.repository.ProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Products Service implementation
 * {@link ProductServiceImpl }
 *
 * @author antonio
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository repository;

    @Autowired
    private ModelMapper modelMapper;


    /**
     * Get Producto Similar
     *
     * @param id
     * @return List<Product>
     */
    public List<Product> getProductsSimilar(Integer id){
        List<Product> productList = new ArrayList<>();
        Integer[] idsSimilar = repository.getProductsSimilar(id);
        Arrays.stream(idsSimilar).parallel().forEach(idSimilar ->{
            ProductEntity entity = repository.getProductDetail(idSimilar);
            if(entity != null) {
                productList.add(modelMapper.map(entity, Product.class));
            }
        });
        return productList;
    }

}
