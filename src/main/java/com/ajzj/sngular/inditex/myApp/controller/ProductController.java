package com.ajzj.sngular.inditex.myApp.controller;

import com.ajzj.sngular.inditex.myApp.model.Product;
import com.ajzj.sngular.inditex.myApp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Products Controller
 *
 * @author antonio
 */
@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    /**
     * Method for
     * get similar products by id
     *
     * @param id
     * @return
     */
    @GetMapping("/product/{productId}/similar")
    public ResponseEntity<List<Product>> getProductsSimilar(@PathVariable("productId") Integer id) {
        return new ResponseEntity<List<Product>>(productService.getProductsSimilar(id), HttpStatus.OK);
    }
}