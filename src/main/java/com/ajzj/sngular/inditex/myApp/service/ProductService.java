package com.ajzj.sngular.inditex.myApp.service;

import com.ajzj.sngular.inditex.myApp.model.Product;

import java.util.List;

/**
 * Products Service
 * {@link ProductService }
 *
 * @author antonio
 */
public interface ProductService {

    /**
     * Get products similar by id
     *
     * @param id
     */
    List<Product> getProductsSimilar(Integer id);

}
