package com.ajzj.sngular.inditex.myApp.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProductEntity implements Serializable {

    private String id;
    private String name;
    private String price;
    private Boolean availability;
}
