package com.ajzj.sngular.inditex.myApp;

import com.ajzj.sngular.inditex.myApp.entity.ProductEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class ContractVerifierBase {


    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    private WireMockServer wireMockServer = new WireMockServer();
    private String PATH_MOCKS = "src/test/resources/contracts/mocks/";

    @BeforeEach
    public void setup() {

        RestAssuredMockMvc.webAppContextSetup(this.context);

        wireMockServer.start();

        try {
            mocksDetailProducts();
            mocksSimilarProducts();

        } catch (IOException e) {
            log.error("Failed mocks IT!!");
        }

    }

    /**
     * Mocks endpoints for detail products
     * @throws IOException
     */
    private void mocksDetailProducts() throws IOException {
        List<Integer> listIds = Arrays.asList(1, 2, 4);
        for(Integer id: listIds) {
            stubFor(get(urlMatching("/product/" + id))
                    .willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
                            .withBody(objectMapper.writeValueAsString(
                                    objectMapper.readValue(
                                            new File(PATH_MOCKS + "get_product_" + id + ".json"), ProductEntity.class)))));

        }
        stubFor(get(urlMatching("/product/3"))
                .willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
                    .withFixedDelay(100)
                    .withBody(objectMapper.writeValueAsString(objectMapper.readValue(
                        new File(PATH_MOCKS + "get_product_3.json"), ProductEntity.class)))));

        stubFor(get(urlMatching("/product/100"))
                .willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
                    .withFixedDelay(5000)
                    .withBody(objectMapper.writeValueAsString(objectMapper.readValue(
                        new File(PATH_MOCKS + "get_product_100.json"), ProductEntity.class)))));
        stubFor(get(urlMatching("/product/1000"))
                .willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
                    .withFixedDelay(5000)
                    .withBody(objectMapper.writeValueAsString(objectMapper.readValue(
                        new File(PATH_MOCKS + "get_product_1000.json"), ProductEntity.class)))));
        stubFor(get(urlMatching("/product/10000"))
                .willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
                    .withFixedDelay(50000)
                    .withBody(objectMapper.writeValueAsString(objectMapper.readValue(
                        new File(PATH_MOCKS + "get_product_10000.json"), ProductEntity.class)))));
        stubFor(get(urlMatching("/product/5"))
                .willReturn(aResponse().withStatus(404).withHeader("Content-Type", "application/json")));

        stubFor(get(urlMatching("/product/6"))
                .willReturn(aResponse().withStatus(500).withHeader("Content-Type", "application/json")));
    }

    /**
     * Mocks endpoints for similar products
     * @throws IOException
     */
    private void mocksSimilarProducts() throws IOException {
        List<Integer> listIds = Arrays.asList(1, 2, 3, 4, 5);
        for(Integer id: listIds) {
            stubFor(get(urlMatching("/product/"+id+"/similarids"))
                .willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
                    .withBody(objectMapper.writeValueAsString(
                                objectMapper.readValue(
                                new File(PATH_MOCKS + "get_similar_"+id+".json"), ProductEntity.class)))));
        }
    }

    /**
     *
     */
    @AfterEach
    public void finish() {
        wireMockServer.stop();
    }
}